import signal
from telegram import ParseMode, InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import Updater, ConversationHandler, CommandHandler, CallbackQueryHandler
from bs4 import BeautifulSoup
import requests
import urllib
import logging
import random
from datetime import datetime, timedelta
from emoji import emojize

def setup_logging():
    lvl = logging.INFO # or DEBUG
    logging.basicConfig(level=lvl, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

def log(*args, **kwargs):
    logging.getLogger('tdm_bot').info(*args, **kwargs)

def md_escape(s):
    """Escape characters in string as required by Telegram markdown v2.
    See https://core.telegram.org/bots/api.
    """
    for char in '_*[]()~`>#+-=|{}.!':
        s = s.replace(char, f'\\{char}')
    return s

def send_article(req, bot, chat_id):
    """
    Reply to `chat_id` with contents of the wiki article contained in `req`.
    """
    title = md_escape(urllib.parse.unquote(req.url.split('/')[-1].replace('_', ' ')))
    link = req.url.replace('https://', '').replace('http://','').replace(')','\\)')
    text = f'[{title}](https://{link})'
    article = BeautifulSoup(req.content, 'lxml').html.body.find(id="bodyContent")
    first_paragraph = article.find('p')
    if first_paragraph is not None:
        text += '\n' + md_escape(first_paragraph.text.strip())

    first_img = article.find('img')
    img = 'https://tanadelmimic.it' + first_img.attrs['src'] if first_img else None
    if img is not None:
        # bypass Telegram's image cache by appending a "unique", useless query string
        img += '?a={int(time())}'

    bot.send_message(chat_id, text, parse_mode=ParseMode.MARKDOWN_V2)
    if img is not None:
        bot.send_photo(chat_id, img)

def get_rnd_page():
    """
    A random page from tanadelmimic.it
    """
    r = requests.get('https://tanadelmimic.it/w/api.php?action=query&format=xml&list=allpages&aplimit=500&apfilterredir=nonredirects')
    if r.status_code != 200:
        raise RuntimeError('could not access tanadelmimic.it')
    all_pages = BeautifulSoup(r.content, 'lxml').html.find_all('p')
    if len(all_pages) >= 500:
        raise RuntimeError('our simple API query does not support wikis larger than 500 pages')
    title = urllib.parse.quote(random.choice(all_pages).attrs['title'])
    r = requests.get(f'https://tanadelmimic.it/wiki/{title}')
    if r.status_code != 200:
        raise RuntimeError('could not get title "{title}" from tanadelmimic.it')
    return r

def search(update, ctx):
    """
    Search a page from tanadelmimic.it
    """
    chat_id = update.message.chat_id

    if len(ctx.args) == 0:
        update.message.reply_markdown(f"Non ho ricevuto nulla da cercare...{emojize(':cry:', use_aliases=True)}",
                                      quote=False)
        return

    search_string = " ".join(ctx.args)
    log(f'searching "{search_string}" for chat {chat_id}')
    r = requests.get('https://tanadelmimic.it/w/index.php?title=Speciale%3ARicerca&search='+search_string)
    if r.status_code != 200:
        raise RuntimeError('could not access tanadelmimic.it')

    soup = BeautifulSoup(r.content, 'lxml').html.body
    nothing_found = soup.find(attrs={'class':'mw-search-nonefound'}) is not None
    if nothing_found:
        update.message.reply_markdown(f"Mi spiace, non ho trovato nulla... {emojize(':cry:', use_aliases=True)}",
                                      quote=False)
        return

    search_results = list(set(soup.find_all(attrs={"class": "mw-search-result"})))
    if len(search_results) == 0:
        # we found a perfect match and the search returned the page itself
        send_article(r, ctx.bot, chat_id)
    elif len(search_results) == 1:
        # if there is no perfect match but just one search result, return that page
        wiki_url = search_results[0].find(attrs={'class':'mw-search-result-heading'}).a.attrs['href']
        r = requests.get(f'https://tanadelmimic.it/{wiki_url}')
        if r.status_code != 200:
            raise RuntimeError('could not get title "{wiki_url.split("/")[-1]}" from tanadelmimic.it')
        send_article(r, ctx.bot, chat_id)
    else:
        # return a keyboard to let user pick one of the search results
        results_data = [s.find(attrs={'class':'mw-search-result-heading'}).a.attrs for s in search_results]
        unique_results = list(set(map(lambda d : (d['href'], d['title']), results_data)))
        sorted_results = sorted(unique_results, key=lambda r: r[1])
        buttons = [InlineKeyboardButton(r[1], callback_data=r[0]) for r in sorted_results]
        response_keyboard = InlineKeyboardMarkup.from_column(buttons)
        update.message.reply_text(
            "Quale di queste pagine stavi cercando, avventuriero?",
            reply_markup=response_keyboard,
            quote=True)

def search_callback(update, ctx):
    page_url = str(update.callback_query.data)
    chat_id = update.callback_query.message.chat_id
    r = requests.get(f'https://tanadelmimic.it/{page_url}')
    log(f'sending article {page_url.split("/")[-1]} to chat {chat_id}')
    if r.status_code != 200:
        raise RuntimeError('could not get title "{page_url}" from tanadelmimic.it')
    send_article(r, ctx.bot, chat_id)

def send_rnd_article(update, ctx):
    chat_id = update.message.chat_id
    log(f'sending article to chat {chat_id}')
    req = get_rnd_page()
    send_article(req, ctx.bot, chat_id)

def start(update, ctx):
    chat_id = update.message.chat_id
    log(f'start in chat {chat_id}')
    update.message.reply_text("""Bevenuto, avventuriero! Usa /search "something" per cercare una pagina della wiki, /rndarticle per ricevere una pagina a caso della wiki, o /dailyarticle per ricevere una pagina a caso ogni giorno a quest'ora (annulla con /stopdaily).""")

def send_article_job(ctx):
    log(f"sending article to chat {ctx.job.context}")
    r = get_rnd_page()
    send_article(r, ctx.bot, chat_id=ctx.job.context)

def schedule_daily(update, ctx):
    chat_id = update.message.chat_id
    log(f"scheduling daily articles to chat {chat_id}")
    time = (datetime.utcnow() + timedelta(seconds=10)).time()
    ctx.job_queue.run_daily(send_article_job, time, context=chat_id, name=str(chat_id))
    update.message.reply_text(f'Aggiunta una "pagina del giorno" alle {time.hour:02}:{time.minute:02} UTC')

def stop_daily(update, ctx):
    chat_id = update.message.chat_id
    log(f'removing job for chat {chat_id}')
    jobs = ctx.job_queue.get_jobs_by_name(chat_id)
    jobs = [j for j in jobs if not j.removed]
    if len(jobs) > 0:
        j = jobs[0]
        hour, minute = j.next_t.hour, j.next_t.minute
        j.schedule_removal()
        update.message.reply_text(f'Cancellata la "pagina del giorno" delle {hour:02}:{minute:02} UTC')
    else:
        update.message.reply_text("Non c'e' nessuna \"pagina del giorno\" da cancellare")

def stop_bot_at_sigint(updater):
    def stop_bot(sig, frame):
        log('SIGINT received, stopping bot (might take a while)')
        updater.stop()
    signal.signal(signal.SIGINT, stop_bot)

def start_bot():
    # create updater: the updater is responsible for the background handling of telegram events
    u = Updater(token='TG_TOKEN', use_context=True)

    # register bot commands
    u.dispatcher.add_handler(CommandHandler('start', start))
    u.dispatcher.add_handler(CommandHandler('rndarticle', send_rnd_article))
    u.dispatcher.add_handler(CommandHandler('search', search))
    u.dispatcher.add_handler(CommandHandler('dailyarticle', schedule_daily, pass_job_queue=True))
    u.dispatcher.add_handler(CommandHandler('stopdaily', stop_daily))
    u.dispatcher.add_handler(CallbackQueryHandler(search_callback))

    # start polling, stop on SIGINT
    stop_bot_at_sigint(u)
    u.start_polling()
    u.idle()

if __name__ == '__main__':
    setup_logging()
    start_bot()
